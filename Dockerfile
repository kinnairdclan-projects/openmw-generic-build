FROM docker.io/library/centos:7.9.2009

RUN yum -y install epel-release \
    && yum -y update \
    && yum -y install automake alsa-lib-devel bison-devel bison bzip2-devel centos-release-scl cmake3 freetype-devel gettext git hg make libtool libarchive libpng-devel libicu-devel libjpeg-turbo-devel librsvg2-devel libSM-devel libtiff-devel libudev-devel libxml2-devel libXcomposite-devel libXt-devel libXrandr-devel lz4-devel mesa-libGL-devel mesa-libGLU-devel mesa-libEGL-devel mesa-libgbm-devel svn xcb-util-wm-devel xcb-util-image-devel xcb-util-keysyms-devel xcb-util-renderutil-devel rpm-build rpmdevtools openssl-devel pulseaudio-libs-devel libffi-devel python3 python3-devel python-devel qt5-qtbase tar expat-devel vim which \
    && yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ \
    && yum -y update

RUN ln -s /usr/bin/cmake3 /usr/local/bin/cmake

COPY build.sh /

VOLUME /opt/openmwbuild

CMD [ "sh", "-c", "time /build.sh" ]
