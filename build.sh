#!/usr/bin/env bash
set -ex
set -o pipefail

#TODO: fix the cursor
#TODO: work within source
#TODO: CI-ID.txt
#TODO: LTO?
#TODO: CHANGELOG.txt ????

# OpenMW version
openmw_version="${OPENMW_VERSION:-openmw-0.47.0}"
openmw_short_version=$(echo ${openmw_version} | sed "s|openmw-||")
if ! [ -z "${openmw_short_version}" ]; then
    build_type="${BUILD_TYPE:-Release}"
    _version="${openmw_short_version}"
else
    build_type="${BUILD_TYPE:-RelWithDebInfo}"
    _version="${openmw_version}"
fi

build_arch="${BUILD_ARCH:-$(uname -m)}"

# Dependency versions and shasums
boost_version=1.73.0
boost_ver=$(echo "${boost_version}" | sed "s|\.|_|g")
boost_sha=4eb3b8d442b426dc35346235c8733b5ae35ba431690e38c6a8263dce9fcbb402

openal_version=1.21.0
openal_sha=cd3650530866f3906058225f4bfbe0052be19e0a29dcc6df185a460f9948feec

nasm_version=2.14.02
nasm_sha=34fd26c70a277a9fdd54cb5ecf389badedaf48047b269d1008fbc819b24e80bc

yasm_version=1.3.0
yasm_sha=3dce6601b495f5b3d45b59f7d2492a340ee7e84b5beca17e48f862502bd5603f

lame_version=3.100
lame_sha=ddfe36cab873794038ae2c1210557ad34857a4b6bdc515785d1da9e175b1da1e

opus_version=1.3.1
opus_sha=65b58e1e25b2a114157014736a3d9dfeaad8d41be1c8179866f144a2fb44ff9d

ogg_version=1.3.4
ogg_sha=fe5670640bd49e828d64d2879c31cb4dde9758681bb664f9bdbf159a01b0c76e

vorbis_version=1.3.6
vorbis_sha=6ed40e0241089a42c48604dc00e362beee00036af2d8b3f46338031c9e0351cb

vpx_version=1.11.0
vpx_sha=965e51c91ad9851e2337aebcc0f517440c637c506f3a03948062e3d5ea129a83

ffmpeg_version=4.2.3
ffmpeg_sha=217eb211c33303b37c5521a5abe1f0140854d6810c6a6ee399456cc96356795e

txc_dxtn_version=1.0.1
txc_dxtn_sha=45290d12cdca529a3f41e7fc35c4250fc1b6d2fc16b56680f8401f6aa792ae60

wayland_version=1.18.0
wayland_sha=4675a79f091020817a98fd0484e7208c8762242266967f55a67776936c2e294d

wayland_protocols_version=1.20
wayland_protocols_sha=9782b7a1a863d82d7c92478497d13c758f52e7da4f197aa16443f73de77e4de7

meson_version=0.54.2
meson_sha=a7716eeae8f8dff002e4147642589ab6496ff839e4376a5aed761f83c1fa0455

ninja_version=1.10.0
ninja_sha=bb489516d71f6e9c01ae65ab177041e025736bfcb042ac037be9e298abfcb056

xkbcommon_version=1.3.1
xkbcommon_sha=8eda6782c6ed4b83296521f2f7e6bea88aba76d49c39fb4fce0f8d355a9181ce

sdl2_version=2.0.14
sdl2_sha=d8215b571a581be1332d2106f8036fcb03d12a70bae01e20f424976d275432bc

libpng16_version=1.6.37
libpng16_sha=505e70834d35383537b6491e7ae8641f1a4bed1876dbfe361201fc80868d88ca
libpng16_patch_sha=823bb2d1f09dc7dae4f91ff56d6c22b4b533e912cbd6c64e8762255e411100b6

qt5_version=5.15.0
qt5_ver=5.15
qt5_sha=9e7af10aece15fa9500369efde69cb220eee8ec3a6818afe01ce1e7d484824c5
qt5_wayland_qpa_sha=084133e10bfbd32a28125639660c59975f23457bba6a79b30a25802cec76a9fb

collada_version=2.5.0
collada_sha=3be672407a7aef60b64ce4b39704b32816b0b28f61ebffd4fbd02c8012901e0d

luajit_version=2.1.0-beta3
luajit_sha=1ad2e34b111c802f9d0cdf019e986909123237a28c746b21295b63c9e785d9c3

unshield_version=1.4.3
unshield_sha=aa8c978dc0eb1158d266eaddcd1852d6d71620ddfc82807fe4bf2e19022b7bab

needed_libs=(
    /usr/lib64/libQt5Concurrent.so.5
    /usr/lib64/libQt5Core.so.5
    /usr/lib64/libQt5DBus.so.5
    /usr/lib64/libQt5Network.so.5
    /usr/lib64/libQt5Xml.so.5
    /usr/lib64/libcrypto.so.10
    /usr/lib64/libexpat.so.1
    /usr/lib64/libfontconfig.so.1
    /usr/lib64/libfreetype.so.6
    /usr/lib64/libglib-2.0.so.0
    /usr/lib64/libgomp.so.1
    /usr/lib64/libgraphite2.so.3
    /usr/lib64/libgthread-2.0.so.0
    /usr/lib64/libharfbuzz.so.0
    /usr/lib64/libicudata.so.50
    /usr/lib64/libicudata.so.50.2
    /usr/lib64/libicui18n.so.50
    /usr/lib64/libicui18n.so.50.2
    /usr/lib64/libicuuc.so.50
    /usr/lib64/libicuuc.so.50.2
    /usr/lib64/libjpeg.so.62
    /usr/lib64/libjpeg.so.62.1.0
    /usr/lib64/liblz4.so.1
    /usr/lib64/liblzma.so.5
    /usr/lib64/libogg.so.0
    /usr/lib64/libpcre.so.1
    /usr/lib64/libpcrecpp.so.0
    /usr/lib64/libpng15.so.15
    /usr/lib64/libssl.so.10
    /usr/lib64/libuuid.so.1
    /usr/lib64/libvorbis.so.0
    /usr/lib64/libvorbisenc.so.2
    /usr/lib64/libxml2.so.2
    /usr/local/lib/libQt5Concurrent.so.5
    /usr/local/lib/libQt5Concurrent.so.5.15.0
    /usr/local/lib/libQt5Core.so.5
    /usr/local/lib/libQt5Core.so.5.15.0
    /usr/local/lib/libQt5DBus.so.5
    /usr/local/lib/libQt5DBus.so.5.15.0
    /usr/local/lib/libQt5EglFSDeviceIntegration.so.5
    /usr/local/lib/libQt5EglFSDeviceIntegration.so.5.15.0
    /usr/local/lib/libQt5EglFsKmsSupport.so.5
    /usr/local/lib/libQt5EglFsKmsSupport.so.5.15.0
    /usr/local/lib/libQt5Gui.so.5
    /usr/local/lib/libQt5Gui.so.5.15.0
    /usr/local/lib/libQt5Network.so.5
    /usr/local/lib/libQt5Network.so.5.15.0
    /usr/local/lib/libQt5OpenGL.so.5
    /usr/local/lib/libQt5OpenGL.so.5.15.0
    /usr/local/lib/libQt5WaylandClient.so.5
    /usr/local/lib/libQt5WaylandClient.so.5.15.0
    /usr/local/lib/libQt5Widgets.so.5
    /usr/local/lib/libQt5Widgets.so.5.15.0
    /usr/local/lib/libQt5XcbQpa.so.5
    /usr/local/lib/libQt5XcbQpa.so.5.15.0
    /usr/local/lib/libQt5Xml.so.5
    /usr/local/lib/libQt5Xml.so.5.15.0
    /usr/local/lib/libSDL2-2.0.so.0
    /usr/local/lib/libavcodec.so.58
    /usr/local/lib/libavformat.so.58
    /usr/local/lib/libavutil.so.56
    /usr/local/lib/libboost_chrono.so.1.73.0
    /usr/local/lib/libboost_date_time.so.1.73.0
    /usr/local/lib/libboost_filesystem.so.1.73.0
    /usr/local/lib/libboost_iostreams.so.1.73.0
    /usr/local/lib/libboost_program_options.so.1.73.0
    /usr/local/lib/libboost_system.so.1.73.0
    /usr/local/lib/libboost_thread.so.1.73.0
    /usr/local/lib/libcollada-dom2.5-dp.so.0
    /usr/local/lib/libmp3lame.so.0
    /usr/local/lib/libogg.so.0
    /usr/local/lib/libopus.so.0
    /usr/local/lib/libpng16.so.16
    /usr/local/lib/libswresample.so.3
    /usr/local/lib/libswscale.so.5
    /usr/local/lib/libtxc_dxtn.so
    /usr/local/lib/libvorbis.so.0
    /usr/local/lib/libvorbisenc.so.2
    /usr/local/lib/libvpx.so.7
    /usr/local/lib/libvpx.so.7.0.0
    /usr/local/lib64/libopenal.so.1
    /usr/local/lib64/libunshield.so.0
)

base_dir=/opt/openmwbuild
deps_dir="${base_dir}/deps"
makeflags="-j $(nproc)"

# Convenience functions
maybe_download() {
    url="${1}"
    if ! [ -f "./$(basename ${url})" ]; then
        curl -L -O "${url}"
    fi
}

maybe_mkdir() {
    target="${1}"
    if ! [ -d "${target}" ]; then
        mkdir -p "${target}"
    fi
}

maybe_rmdir() {
    target="${1}"
    if [ -d "${target}" ] && [ -z $NO_FORCE_CLEAN ]; then
        rm -rf "${target}"
    fi
}

maybe_rm() {
    target="${1}"
    if [ -f "${target}" ] && [ -z $NO_FORCE_CLEAN ]; then
        rm -f "${target}"
    fi
}

#
# Setup
#
if ! [ -z $DO_YUM_INSTALLS ]; then
    #
    # Deps from repos:
    #
    #add epel repo
    yum -y epel-release
    yum -y update
    yum -y install automake alsa-lib-devel bison-devel bison bzip2-devel centos-release-scl cmake3 freetype-devel gettext git hg make libtool libarchive libpng-devel libicu-devel libjpeg-turbo-devel librsvg2-devel libSM-devel libtiff-devel libudev-devel libxml2-devel libXcomposite-devel libXt-devel libXrandr-devel lz4-devel mesa-libGL-devel mesa-libGLU-devel mesa-libEGL-devel mesa-libgbm-devel svn xcb-util-wm-devel xcb-util-image-devel xcb-util-keysyms-devel xcb-util-renderutil-devel rpm-build rpmdevtools openssl-devel pulseaudio-libs-devel libffi-devel python3 python3-devel python-devel qt5-qtbase tar expat-devel vim which
    # newer gcc/g++ from devtoolset
    yum -y  install devtoolset-9-gcc devtoolset-9-gcc-c++
    # make sure newest packages are installed
    yum -y update

    # make cmake3 available as cmake
    ln -s /usr/bin/cmake3 /usr/local/bin/cmake
fi

#enable devtoolset
source /opt/rh/devtoolset-9/enable
rpmdev-setuptree
echo "PKG_CONFIG_PATH=\"/usr/local/lib64/pkgconfig:/usr/local/share/pkgconfig/:/usr/local/lib/pkgconfig/:/usr/lib/pkgconfig\";export PKG_CONFIG_PATH" >> ~/.bashrc
source ~/.bashrc
maybe_mkdir ${deps_dir}

#
# boost:
#
maybe_mkdir ${deps_dir}/boost
pushd ${deps_dir}/boost
maybe_download https://downloads.sourceforge.net/project/boost/boost/${boost_version}/boost_${boost_ver}.tar.bz2
sha256sum --quiet -c <(echo "${boost_sha}  boost_${boost_ver}.tar.bz2")
maybe_rmdir boost_${boost_ver}
tar -xf boost_${boost_ver}.tar.bz2
pushd boost_${boost_ver}
./bootstrap.sh
./b2
./b2 install

#
# openal: 
#
maybe_mkdir ${deps_dir}/openal
pushd ${deps_dir}/openal
maybe_download https://github.com/kcat/openal-soft/archive/refs/tags/openal-soft-${openal_version}.tar.gz
sha256sum --quiet -c <(echo "${openal_sha}  openal-soft-${openal_version}.tar.gz")
maybe_rmdir openal-soft-openal-soft-${openal_version}
tar -xf openal-soft-${openal_version}.tar.gz
pushd openal-soft-openal-soft-${openal_version}
maybe_mkdir build
pushd build
cmake -DALSOFT_EXAMPLES=OFF -DALSOFT_TESTS=OFF -DALSOFT_UTILS=OFF ..
make ${makeflags}
make install

#
# [[FFMPEG (https://trac.ffmpeg.org/wiki/CompilationGuide/Centos)]]
#

#
# nasm:
#
maybe_mkdir ${deps_dir}/nasm
pushd ${deps_dir}/nasm
maybe_download http://www.nasm.us/pub/nasm/releasebuilds/${nasm_version}/nasm-${nasm_version}.tar.bz2
sha256sum --quiet -c <(echo "${nasm_sha}  ${deps_dir}/nasm/nasm-${nasm_version}.tar.bz2")
maybe_rmdir nasm-${nasm_version}
tar -xf nasm-${nasm_version}.tar.bz2
pushd nasm-${nasm_version}
./autogen.sh
./configure
make ${makeflags}
make install

#
# yasm:
#
maybe_mkdir ${deps_dir}/yasm
pushd ${deps_dir}/yasm
maybe_download http://www.tortall.net/projects/yasm/releases/yasm-${yasm_version}.tar.gz
sha256sum --quiet -c <(echo "${yasm_sha}  yasm-${yasm_version}.tar.gz")
maybe_rmdir yasm-${yasm_version}
tar -xf yasm-${yasm_version}.tar.gz
pushd yasm-${yasm_version}
./configure
make ${makeflags}
make install

#
# lame:
#
maybe_mkdir ${deps_dir}/lame
pushd ${deps_dir}/lame
maybe_download http://downloads.sourceforge.net/project/lame/lame/${lame_version}/lame-${lame_version}.tar.gz
sha256sum --quiet -c <(echo "${lame_sha}  lame-${lame_version}.tar.gz")
maybe_rmdir lame-${lame_version}
tar -xf lame-${lame_version}.tar.gz
pushd lame-${lame_version}
./configure --disable-static --enable-nasm
make ${makeflags}
make install

#
# libopus:
#
maybe_mkdir ${deps_dir}/opus
pushd ${deps_dir}/opus
maybe_download https://archive.mozilla.org/pub/opus/opus-${opus_version}.tar.gz
sha256sum --quiet -c <(echo "${opus_sha}  opus-${opus_version}.tar.gz")
maybe_rmdir opus-${opus_version}
tar -xf opus-${opus_version}.tar.gz
pushd opus-${opus_version}
./configure --disable-static
make ${makeflags}
make install

#
# libogg:
#
maybe_mkdir ${deps_dir}/ogg
pushd ${deps_dir}/ogg
maybe_download http://downloads.xiph.org/releases/ogg/libogg-${ogg_version}.tar.gz
sha256sum --quiet -c <(echo "${ogg_sha}  libogg-${ogg_version}.tar.gz")
maybe_rmdir libogg-${ogg_version}
tar -xf libogg-${ogg_version}.tar.gz
pushd libogg-${ogg_version}
./configure --disable-static
make ${makeflags}
make install

#
# libvorbis:
#
maybe_mkdir ${deps_dir}/vorbis
pushd ${deps_dir}/vorbis
maybe_download http://downloads.xiph.org/releases/vorbis/libvorbis-${vorbis_version}.tar.gz
sha256sum --quiet -c <(echo "${vorbis_sha}  libvorbis-${vorbis_version}.tar.gz")
maybe_rmdir libvorbis-${vorbis_version}
tar -xf libvorbis-${vorbis_version}.tar.gz
pushd libvorbis-${vorbis_version}
./configure --disable-static
make ${makeflags}
make install

#
# libvpx:
#
maybe_mkdir ${deps_dir}/vpx
pushd ${deps_dir}/vpx
maybe_download https://github.com/webmproject/libvpx/archive/refs/tags/v${vpx_version}.tar.gz
sha256sum --quiet -c <(echo "${vpx_sha} v${vpx_version}.tar.gz")
maybe_rmdir libvpx-${vpx_version}
tar -xf v${vpx_version}.tar.gz
pushd libvpx-${vpx_version}
./configure --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm --disable-static --enable-shared --enable-pic
make ${makeflags}
make install

#
# ffmpeg:
#
maybe_mkdir ${deps_dir}/ffmpeg
pushd ${deps_dir}/ffmpeg
maybe_download https://ffmpeg.org/releases/ffmpeg-${ffmpeg_version}.tar.bz2
sha256sum --quiet -c <(echo "${ffmpeg_sha}  ffmpeg-${ffmpeg_version}.tar.bz2")
maybe_rmdir ffmpeg-${ffmpeg_version}
tar -xf ffmpeg-${ffmpeg_version}.tar.bz2
pushd ffmpeg-${ffmpeg_version}
./configure --enable-gpl --enable-libfreetype --enable-libmp3lame --enable-libopus --enable-libvorbis --enable-libvpx --disable-static --enable-shared
make ${makeflags}
make install
#
# [[/FFMPEG]]
#

#
# libtxc_dxtn:
#
maybe_mkdir ${deps_dir}/txc_dxtn
pushd ${deps_dir}/txc_dxtn
maybe_download http://people.freedesktop.org/~cbrill/libtxc_dxtn/libtxc_dxtn-${txc_dxtn_version}.tar.bz2
sha256sum --quiet -c <(echo "${txc_dxtn_sha}  libtxc_dxtn-${txc_dxtn_version}.tar.bz2")
maybe_rmdir libtxc_dxtn-${txc_dxtn_version}
tar -xf libtxc_dxtn-${txc_dxtn_version}.tar.bz2
pushd libtxc_dxtn-${txc_dxtn_version}
./configure
make ${makeflags}
make install

#
# wayland
#
maybe_mkdir ${deps_dir}/wayland
pushd ${deps_dir}/wayland
maybe_download https://wayland.freedesktop.org/releases/wayland-${wayland_version}.tar.xz
sha256sum --quiet -c <(echo "${wayland_sha}  wayland-${wayland_version}.tar.xz")
maybe_rmdir wayland-${wayland_version}
tar -xf wayland-${wayland_version}.tar.xz
pushd wayland-${wayland_version}
EXPAT_CFLAGS="-I/usr/include" EXPAT_LIBS="-L/usr/lib64/ -lexpat" ./configure --disable-static --disable-documentation
make ${makeflags}
make install
popd

maybe_download https://wayland.freedesktop.org/releases/wayland-protocols-${wayland_protocols_version}.tar.xz
sha256sum --quiet -c <(echo "${wayland_protocols_sha}  wayland-protocols-${wayland_protocols_version}.tar.xz")
maybe_rmdir wayland-protocols-${wayland_protocols_version}
tar -xf wayland-protocols-${wayland_protocols_version}.tar.xz
pushd wayland-protocols-${wayland_protocols_version}
./configure
make install
popd

# meson and ninja required for building libxkbcommon
maybe_rmdir meson-${meson_version}
maybe_download https://github.com/mesonbuild/meson/releases/download/${meson_version}/meson-${meson_version}.tar.gz
sha256sum --quiet -c <(echo "${meson_sha}  meson-${meson_version}.tar.gz")
tar -xf meson-${meson_version}.tar.gz

maybe_rmdir ninja-${ninja_version}
maybe_download https://github.com/martine/ninja/archive/v${ninja_version}.zip
sha256sum --quiet -c <(echo "${ninja_sha}  v${ninja_version}.zip")
unzip v${ninja_version}.zip
pushd ninja-${ninja_version}
python3 configure.py --bootstrap

maybe_mkdir ${deps_dir}/libxkbcommon
pushd ${deps_dir}/libxkbcommon
maybe_download https://github.com/xkbcommon/libxkbcommon/archive/refs/tags/xkbcommon-${xkbcommon_version}.tar.gz
sha256sum --quiet -c <(echo "${xkbcommon_sha}  xkbcommon-${xkbcommon_version}.tar.gz")
maybe_rmdir libxkbcommon-xkbcommon-${xkbcommon_version}
tar -xf xkbcommon-${xkbcommon_version}.tar.gz
pushd libxkbcommon-xkbcommon-${xkbcommon_version}
PATH="$PATH:${deps_dir}/wayland/ninja-${ninja_version}"
python3 ${deps_dir}/wayland/meson-${meson_version}/meson.py build -Denable-docs=false
ninja -C build
python3 ${deps_dir}/wayland/meson-${meson_version}/meson.py install -C build

#
# sdl2:
#
maybe_mkdir ${deps_dir}/sdl2
pushd ${deps_dir}/sdl2
maybe_download https://libsdl.org/release/SDL2-${sdl2_version}.tar.gz
sha256sum --quiet -c <(echo "${sdl2_sha}  SDL2-${sdl2_version}.tar.gz")
maybe_rmdir SDL2-${sdl2_version}
tar -xf SDL2-${sdl2_version}.tar.gz
pushd SDL2-${sdl2_version}
./configure --disable-static
make ${makeflags}
make install

#
# libpng16
#
maybe_mkdir ${deps_dir}/libpng16
pushd ${deps_dir}/libpng16
maybe_download https://downloads.sourceforge.net/sourceforge/libpng/libpng-${libpng16_version}.tar.xz
sha256sum --quiet -c <(echo "${libpng16_sha}  libpng-${libpng16_version}.tar.xz")
maybe_download https://downloads.sourceforge.net/sourceforge/libpng-apng/libpng-${libpng16_version}-apng.patch.gz
sha256sum --quiet -c <(echo "${libpng16_patch_sha}  libpng-${libpng16_version}-apng.patch.gz")
maybe_rmdir libpng-${libpng16_version}
tar -xf libpng-${libpng16_version}.tar.xz
pushd libpng-${libpng16_version}
gzip -cd ../libpng-${libpng16_version}-apng.patch.gz | patch -Np1
./configure --disable-static
make ${makeflags}
make install

#
# qt5:
#
maybe_mkdir ${deps_dir}/qt5
pushd ${deps_dir}/qt5
maybe_download https://download.qt.io/official_releases/qt/${qt5_ver}/${qt5_version}/submodules/qtbase-everywhere-src-${qt5_version}.tar.xz
sha256sum --quiet -c <(echo "${qt5_sha}  qtbase-everywhere-src-${qt5_version}.tar.xz")
maybe_rmdir qtbase-everywhere-src-${qt5_version}
tar -xf qtbase-everywhere-src-${qt5_version}.tar.xz
pushd qtbase-everywhere-src-${qt5_version}
./configure -prefix /usr/local -headerdir /usr/local/include/qt5 -opensource -confirm-license -qt-harfbuzz -fontconfig -no-use-gold-linker -no-mimetype-database -nomake examples -shared
make ${makeflags}
make install

#
# qt5 wayland qpa:
#
pushd ${deps_dir}/qt5
maybe_download https://download.qt.io/official_releases/qt/${qt5_ver}/${qt5_version}/submodules/qtwayland-everywhere-src-${qt5_version}.tar.xz
sha256sum --quiet -c <(echo "${qt5_wayland_qpa_sha}  qtwayland-everywhere-src-${qt5_version}.tar.xz")
maybe_rmdir qtwayland-everywhere-src-${qt5_version}
tar -xf qtwayland-everywhere-src-${qt5_version}.tar.xz
pushd qtwayland-everywhere-src-${qt5_version}
qmake
make ${makeflags}
# manually generate missing wayland headers
export LD_LIBRARY_PATH="${deps_dir}/qt5/qtbase-everywhere-src-${qt5_version}/lib/"
./bin/qtwaylandscanner server-header src/extensions/qt-texture-sharing-unstable-v1.xml > ${deps_dir}/qt5/qtwayland-everywhere-src-${qt5_version}/src/compositor/qwayland-server-qt-texture-sharing-unstable-v1.h
wayland-scanner -s server-header src/extensions/qt-texture-sharing-unstable-v1.xml ${deps_dir}/qt5/qtwayland-everywhere-src-${qt5_version}/src/compositor/wayland-qt-texture-sharing-unstable-v1-server-protocol.h
make install

#
# collada
#
maybe_mkdir ${deps_dir}/collada
pushd ${deps_dir}/collada
maybe_download https://github.com/rdiankov/collada-dom/archive/refs/tags/v${collada_version}.tar.gz
sha256sum --quiet -c <(echo "3be672407a7aef60b64ce4b39704b32816b0b28f61ebffd4fbd02c8012901e0d  v${collada_version}.tar.gz")
maybe_rmdir collada-dom-${collada_version}
tar -xf v${collada_version}.tar.gz
pushd collada-dom-${collada_version}
cmake .
make ${makeflags}
make install

#
# luajit
#
maybe_mkdir ${deps_dir}/luajit
pushd ${deps_dir}/luajit
maybe_download https://luajit.org/download/LuaJIT-${luajit_version}.tar.gz
sha256sum --quiet -c <(echo "${luajit_sha}  LuaJIT-${luajit_version}.tar.gz")
maybe_rmdir LuaJIT-${luajit_version}
tar -xf LuaJIT-${luajit_version}.tar.gz
pushd LuaJIT-${luajit_version}
make amalg
make install

#
# unshield:
#
maybe_mkdir ${deps_dir}/unshield
pushd ${deps_dir}/unshield
maybe_download https://github.com/twogood/unshield/archive/${unshield_version}.tar.gz
sha256sum --quiet -c <(echo "${unshield_sha}  ${unshield_version}.tar.gz")
maybe_rmdir unshield-${unshield_version}
tar -xf ${unshield_version}.tar.gz
pushd unshield-${unshield_version}
cmake -DCMAKE_BUILD_TYPE=Release
make ${makeflags}
make install

strip /usr/local/lib{,64}/*.so
strip /usr/local/lib{,64}/*.so.*

#
# openmw
#

maybe_rm "${base_dir}/openmw-${openmw_version}-linux-${build_arch}.tar.gz"
maybe_rm "${base_dir}/openmw-${openmw_version}-linux-${build_arch}.tar.gz.sha256sum.txt"
maybe_rm "${base_dir}/${openmw_version}-linux-${build_arch}.tar.gz"
maybe_rm "${base_dir}/${openmw_version}-linux-${build_arch}.tar.gz.sha256sum.txt"
maybe_rmdir "${base_dir}/openmw-${openmw_version}-linux-${build_arch}"
maybe_rmdir "${base_dir}/${openmw_version}-linux-${build_arch}"
maybe_rmdir ${base_dir}/build

maybe_mkdir ${base_dir}/build/lib

pushd ${base_dir}
maybe_rmdir "openmw-${openmw_version}"
maybe_rmdir "openmw-${_version}"

maybe_download "https://gitlab.com/OpenMW/openmw/-/archive/openmw-${_version}/openmw-${openmw_version}.tar.gz"
tar -xvf "openmw-${openmw_version}.tar.gz"

cp -v "openmw-${openmw_version}/files/mygui/DejaVuFontLicense.txt" build/
cp -v "openmw-${openmw_version}/LICENSE" build/LICENSE.txt

pushd ${base_dir}/build
cmake \
      -DCMAKE_BUILD_TYPE=${build_type} \
      -DBUILD_ESSIMPORTER=OFF \
      -DBUILD_NIFTEST=ON \
      -DOPENMW_USE_SYSTEM_MYGUI=OFF \
      -DOPENMW_USE_SYSTEM_OSG=OFF \
      -DOPENMW_USE_SYSTEM_BULLET=OFF \
      -DOPENMW_USE_SYSTEM_SQLITE3=OFF \
      "../openmw-${openmw_version}"
make ${makeflags}

# Copy needed libs to be bundled
for f in "${needed_libs[@]}"; do
    cp -v $f lib/
done

cp -r /usr/local/plugins .

# Create helper scripts for running OpenMW and OpenMW-CS
for s in bsatool esmtool niftest openmw openmw-cs openmw-iniimporter openmw-wizard openmw-launcher; do
    mv -v "${s}" "${s}.${build_arch}"
    cat > "${s}" <<EOF
#!/bin/sh
# OpenMW Generic Linux ${build_arch} Build (${_version})
readlink() {
	path=\$1
	if [ -L "\$path" ]
	then
		ls -l "\$path" | sed 's/^.*-> //'
	else
		return 1
	fi
}
SCRIPT="\$0"
COUNT=0
while [ -L "\${SCRIPT}" ]
do
	SCRIPT=\$(readlink \${SCRIPT})
	COUNT=\$(expr \${COUNT} + 1)
	if [ \${COUNT} -gt 100 ]
	then
		echo "Too many symbolic links"
		exit 1
	fi
done
GAMEDIR=\$(dirname "\${SCRIPT}")
cd \$GAMEDIR
export LD_LIBRARY_PATH="./lib"
./${s}.${build_arch} "\$@"
EOF
    chmod +x "${s}"
done

cat > README.txt <<EOF
# OpenMW - Linux ${build_arch} (${_version})

This package contains Linux binaries for OpenMW and its suite of utilities. It
is intended to run both on most moderately old and recent Linux distributions.
Basically anything as old as or newer than CentOS 7 (circa 2014).

The binaries depend on libraries that might not be installed on your system or
in an incompatible version. Therefore these libraries are included in the "lib"
subdirectory. To automatically use the provided libraries there are scripts
which set the LD_LIBRARY_VARIABLE to the "lib" directory.

To run openmw or an utility simply run the provided shell script (the file
without a suffix -> <filename> not <filename>.${build_arch}). In case of openmw this
would be "openmw" not openmw.${build_arch}.

If you need help, please visit our support forums:

https://forum.openmw.org/viewforum.php?f=8

Or you may join our Discord server:

https://discord.gg/Xqq8WeJaMu

EOF

# Clean up
for f in CMakeFiles _deps apps components docs extern files CMakeCache.txt Makefile cmake_install.cmake compile_commands.json openmw.appdata.xml; do
    rm -fr ${f}
done

popd
mv -v build "openmw-${_version}-linux-${build_arch}"
tar -czpvf "openmw-${_version}-linux-${build_arch}.tar.gz" "openmw-${_version}-linux-${build_arch}"
sha256sum "openmw-${_version}-linux-${build_arch}.tar.gz" > "openmw-${_version}-linux-${build_arch}.tar.gz.sha256sum.txt"

echo Done!
