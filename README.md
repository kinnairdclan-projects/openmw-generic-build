# openmw-generic-linux-build

## Usage

run `make` to build an image using `podman`, then run it. All files will be placed into this repo's `out` dir.

## Credits

Extra special thanks to K1ll, who wrote the initial version of the build script and also helped me with problems along the way. This would not be possible without you!
